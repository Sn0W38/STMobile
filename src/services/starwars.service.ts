// Core Components
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// lib rxjs
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class StarWarsService {

    constructor(private http: HttpClient) { }

    //Api url
    private base = 'https://swapi.co/api/';

    //select all movie
    getMovie(): Observable<any>
    {
        return this.http.get<any>(this.base + 'films/');
    }

    //select all people per page
    getPeople(page): Observable<any>
    {
        return this.http.get<any>(this.base + 'people/?page='+ page);
    }

    //select all ship per page
    getShip(page): Observable<any>
    {
        return this.http.get<any>(this.base + 'starships/?page='+ page);
    }

    //select all planet per page
    getPlanets(page): Observable<any>
    {
        return this.http.get<any>(this.base + 'planets/?page='+ page);
    }

    //select all species per page
    getSpecies(page): Observable<any>
    {
        return this.http.get<any>(this.base + 'species/?page='+ page);
    }

    //select all vehicles per page
    getVehicles(page): Observable<any>
    {
        return this.http.get<any>(this.base + 'vehicles/?page='+ page);
    }
}