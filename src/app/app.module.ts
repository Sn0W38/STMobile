import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

// Services
import { StarWarsService } from '../services/starwars.service';

// Pages
import { HomePage } from '../pages/home/home';
import { HomeSelectPage } from '../pages/homeSelect/homeSelect';
import { MoviePage } from '../pages/movie/movie';
import { PeoplePage } from '../pages/people/people';
import { StarshipPage } from '../pages/starship/starship';
import { PlanetPage } from '../pages/planet/planet';
import { SpeciesPage } from '../pages/species/species';
import { VehiclesPage } from '../pages/vehicles/vehicles';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    HomeSelectPage,
    MoviePage,
    PeoplePage,
    StarshipPage,
    PlanetPage,
    SpeciesPage,
    VehiclesPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    HomeSelectPage,
    MoviePage,
    PeoplePage,
    StarshipPage,
    PlanetPage,
    SpeciesPage,
    VehiclesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    StarWarsService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
