import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StarWarsService } from '../../services/starwars.service';

@Component({
  selector: 'page-vehicles',
  templateUrl: 'vehicles.html'
})
export class VehiclesPage {

    data: any;
    dataAlls: any[];
    dataCount: string;
    page = 1;

    constructor(public navCtrl: NavController,
                private starWarsService: StarWarsService)
    {
        this.starWarsService.getVehicles(this.page)
        .subscribe((data => {
            this.dataAlls = data['results'];
            this.dataCount = data['count'];
        }))
    }

    doInfinite(infiniteScroll) {
        this.page = this.page+1;
        setTimeout(() => {
            this.starWarsService.getVehicles(this.page)
            .subscribe((data => {
                this.data = data['results'];

                for(let i=0; i<this.data.length; i++)
                {
                    this.dataAlls.push(this.data[i]);
                }
            }))
            infiniteScroll.complete();    
        }, 1000);
    }

}
