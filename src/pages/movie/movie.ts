import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StarWarsService } from '../../services/starwars.service';

@Component({
  selector: 'page-movie',
  templateUrl: 'movie.html'
})
export class MoviePage {

    dataAlls: any;
    dataCount: string;

    constructor(public navCtrl: NavController,
                private starWarsService: StarWarsService)
    {
        this.starWarsService.getMovie()
        .subscribe((data => {
            this.dataAlls = data['results'];
            this.dataCount = data['count'];
        }))
    }

}
