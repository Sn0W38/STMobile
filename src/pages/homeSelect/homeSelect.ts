import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// Page
import { MoviePage } from '../movie/movie';
import { PeoplePage } from '../people/people';
import { StarshipPage } from '../starship/starship';
import { PlanetPage } from '../planet/planet';
import { SpeciesPage } from '../species/species';
import { VehiclesPage } from '../vehicles/vehicles';

@Component({
  selector: 'page-homeSelect',
  templateUrl: 'homeSelect.html'
})
export class HomeSelectPage {

    constructor(public navCtrl: NavController){ }

    movieAction(): void 
    {
        this.navCtrl.push(MoviePage);
    }

    peopleAction(): void 
    {
        this.navCtrl.push(PeoplePage);
    }

    starshipAction(): void 
    {
        this.navCtrl.push(StarshipPage);
    }

    planetAction(): void 
    {
        this.navCtrl.push(PlanetPage);
    }

    speciesAction(): void 
    {
        this.navCtrl.push(SpeciesPage);
    }

    vehiclesAction(): void 
    {
        this.navCtrl.push(VehiclesPage);
    }
}
