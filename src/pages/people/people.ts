import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StarWarsService } from '../../services/starwars.service';

@Component({
  selector: 'page-people',
  templateUrl: 'people.html'
})
export class PeoplePage {

    //variable
    data: any;
    dataAlls: any[];
    dataCount: string;
    page = 1;

    constructor(public navCtrl: NavController,
        private starWarsService: StarWarsService)
    {
        //Start Data for loading router
        this.starWarsService.getPeople(this.page)
        .subscribe((data => {
                //people's data
                this.data = data['results'];
                this.dataAlls = this.data;
                //people's count
                this.dataCount = data['count'];
        }))
    }

    //Scroll infinite
    doInfinite(infiniteScroll)
    {
        this.page = this.page+1;
        setTimeout(() => {
            this.starWarsService.getPeople(this.page)
            .subscribe((data => {
                this.data = data['results'];

                for(let i=0; i<this.data.length; i++)
                {
                    //add data on the end array
                    this.dataAlls.push(this.data[i]);
                }
            }))
            infiniteScroll.complete();    
        }, 1000);
    }
}
