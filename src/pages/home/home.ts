import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HomeSelectPage } from '../homeSelect/homeSelect';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    constructor(public navCtrl: NavController){ }

    showHomeSelect()
    {
        this.navCtrl.push(HomeSelectPage);
    }

    slides = [
        {
            title: "Welcome to Star Wars Archive",
            description: "This project is created for obtain my graduate",
            image: "assets/imgs/diplome.png",
        },
        {
            description: 'This application is based on API Star Wars : <a href="https://swapi.co/02">SWAPI</a>, ionic, Angular2',
            image: "assets/imgs/ica.png",
        }
    ];
}
